import {NotificationManager} from "react-notifications";

export const toastSuccessMessage = (message)=>{
    NotificationManager.success(message, "", 2000)
}
export const toastErrorMessage = (message)=>{
    NotificationManager.error(message, "", 2000)
}