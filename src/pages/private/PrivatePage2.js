import React from "react"
import {Link} from "react-router-dom";

const PrivatePage2 = ()=>{
    return (
        <div>
            <h1>2: only accept after login</h1>
            <Link to="/admin">to admin page 1</Link><br/><br/>
        </div>
    )
}

export default PrivatePage2;