import React from "react"
import {removeToken} from "../../utils/auth";
import {Link, useNavigate} from "react-router-dom";
import {toastSuccessMessage} from "../../utils/notifications";

const PrivatePageExample = ()=>{
    const navigate = useNavigate()
    const handleLogout = ()=>{
        removeToken()
        toastSuccessMessage("Logout success")
        navigate({
            pathname : "/login"
        })
    }
    return (
        <div>
            <h1>only accept after login</h1>
            <Link to="/admin/desk">to admin page 2</Link><br/><br/>
            <button onClick={handleLogout}>Logout</button>
        </div>
    )
}

export default PrivatePageExample;