import React, {useState} from "react"
import {useDispatch} from "react-redux";
import {toastErrorMessage, toastSuccessMessage} from "../../utils/notifications";
import {login} from "../../store/actions/user-action";
import {Navigate, useNavigate} from "react-router-dom";
import {getToken} from "../../utils/auth";
import {Button, Checkbox, Form, Input} from 'antd';
import "../auth/style.css"
import { Card } from 'antd';
const LoginPage = () => {
    const LoginTabRender = <div className="login-tab">Đăng nhập</div>;
    // const SignupTabRender = <div className="login-tab">Signup</div>;
    const tabList = [
        {
            key: 'loginPage',
            tab: LoginTabRender,
        },
    ];
    const token = getToken()
    const navigate = useNavigate();
    const dispatch = useDispatch();
    const [loginForm, setLoginForm] = useState({
        username : "",
        password : ""
    })

    const handleLogin = () => {
        const data = {
            username: loginForm.username,
            password: loginForm.password
        }
        const onSuccess = () => {
            toastSuccessMessage("login success")
            navigate({
                pathname: "/admin"
            })
        }
        const onError = (message) => {
            toastErrorMessage(message)
        }
        dispatch(login(data, onSuccess, onError))
    }


    if (token) {
        return <Navigate to="/admin"/>
    }


    return (
        <div className="login-background">
            <Card
                className="login-form"
                size="default"
                tabList={tabList}
            >
                <Form
                    name="Login form"
                    layout={'vertical'}
                    style={{
                        maxWidth: '500px',
                    }}
                    fields={[
                        {name : "username", value : loginForm.username},
                        {name : "password", value : loginForm.password},
                    ]}
                    onFinish={handleLogin}
                    autoComplete="off"
                >
                    <Form.Item
                        label="Tài khoản"
                        name="username"
                        max={10}
                        rules={[{ required: true, message: 'Please input your username!' }]}
                    >
                        <Input onChange={(e)=> setLoginForm({...loginForm, username : e.target.value})}/>
                    </Form.Item>

                    <Form.Item
                        label="Mật khẩu"
                        name="password"
                        rules={[{ required: true, message: 'Please input your password!' }]}
                    >
                        <Input.Password onChange={(e)=> setLoginForm({...loginForm, password : e.target.value})}/>
                    </Form.Item>

                    <Form.Item className="text-end">
                        <Button type="primary" htmlType="submit">
                            Submit
                        </Button>
                    </Form.Item>
                </Form>
            </Card>
        </div>
    )
}

export default LoginPage;