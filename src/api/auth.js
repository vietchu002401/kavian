import request from "../utils/request"

export const login = (payload)=>{
    return request({
        url : "/api/auth/login",
        method : "POST",
        data : payload
    })
}

export const regíter = (payload)=>{
    return request({
        url : "/api/auth/admin/register",
        method : "POST",
        data : payload
    })
}

export const changePassword = (payload)=>{
    return request({
        url : "/api/auth/change-password",
        method : "POST",
        data : payload
    })
}